///
/// \file eval_ad9958_esp32.ino
/// \brief Main file
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2023, Dmitry Kuklin
/// \license GPLv3
///


#include <Arduino.h>


#define LED 2


void setup(void)
{
    Serial.begin(115200);
    pinMode(LED, OUTPUT);

    initSpi();

    Serial.println("Init AD9958");
    initAD9958();

    Serial.println("Set 250 kHz for channel 1");
    setFrequencyAD9958(1, 250000);

    Serial.println("Set amnplitude 0.5 for channel 1");
    setAmplitudeAD9958(1, 0.5);
}


void loop(void)
{
    digitalWrite(LED, HIGH);
    delay(1000);
    digitalWrite(LED, LOW);
    delay(1000);
}
